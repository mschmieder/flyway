# dowloads and installs flyway
class { "flyway" :
  version => '3.2.1',
  path => '/tools/fway/',
  add_to_path => false,
}

::flyway::config { "flyway.conf" :
  path => join(["$::flyway::installer::install_dir","/flyway.conf"]),
  params => {
    url => 'jdbc:mysql://localhost:3306/haccp',
    user => 'root',
    password => 'mysql'
  },
  require => Class['flyway']
}

::flyway::migrate { 'flyway_migrations':
  source => "puppet:///database/sql",
  target => join(["$::flyway::installer::install_dir","/sql"]),
  recurse => remote,  
  require => Flyway::Config['flyway.conf']
}

notice("install path: $::flyway::installer::install_dir")


  # run some the flyway executable and create a file 
exec { 'flyway_path_test':
    command     => 'flyway >> /tmp/flywayversion',
    path        => "/usr/bin:/usr/sbin:/bin:/usr/local/bin:$::flyway::installer::install_dir",
    require     => Class['flyway']
  }->file{ '/tmp/flywayversion' :
     ensure => present 
  }->exec { 'delete_tmp_file':
    command      => 'rm -f /tmp/flywayversion',
    path        => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
  }
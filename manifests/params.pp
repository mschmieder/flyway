class flyway::params {

 # setup flyway installation path
 $path = $operatingsystem ? {
    /(Ubuntu|Debian)/        => '/tools/flyway',
 }
 
 $download_dir =  $operatingsystem ? {
    /(Ubuntu|Debian)/ => "/tmp/",
 }

 $version = '3.2.1'
 
 $path_config_file = $operatingsystem ? {
    /(Ubuntu|Debian)/  => '/etc/profile',
 }

 $add_to_path = true

}

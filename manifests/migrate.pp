define flyway::migrate (
  $source = {},
  $target = {},
  $recurse = {} ) 

{
  include ::flyway::installer

  notice("Executing flyway migrations.. ")
  notice(".. source : ${source}")
  notice(".. bin path : $::flyway::installer::install_dir")


  file { "${source}":
    path => "${target}",
    ensure => 'directory',
    source => "${source}",
    recurse => $recurse,
  }

  # run some the flyway executable and create a file 
  exec { "migrate_${source}" :
    command     => 'flyway migrate',
    cwd         => "$::flyway::installer::install_dir",
    path        => "/usr/bin:/usr/sbin:/bin:/usr/local/bin:$::flyway::installer::install_dir",
    require     => File["${source}"]
  }
}
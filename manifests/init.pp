class flyway(
    $path = $::flyway::path,
    $version = $::flyway::version,
    $add_to_path = $::flyway::params::add_to_path
) inherits ::flyway::params {

  class { 'flyway::installer' :
    path => $path,
    version => $version,
    add_to_path => $add_to_path
  }
}
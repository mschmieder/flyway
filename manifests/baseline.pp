define flyway::baseline (
  $source = { },
  $target = { },
  $recurse = { },
  $baseline = undef,
  $baselineDescription = undef
) {
  include ::flyway::installer

  notice("Executing flyway baseline.. ")
  notice(".. baseline : ${baseline}")
  notice(".. baselineDescription : ${baselineDescription}")

  # validate that the baseline has been set and the description
  if $baseline == undef {
    fail ("The baseline parameter has to be set for flyway::baseline !")
  }

  if $baselineDescription == undef {
    fail ("The baselineDescription parameter has to be set for flyway::baseline !")
  }

  # run some the flyway executable and create a file
  exec { 'flyway_path_test':
    command     => "flyway baseline -baselineVersion=${baseline} -baselineDescription=${baselineDescription} ",
    cwd         => "$::flyway::installer::install_dir",
    path        => "/usr/bin:/usr/sbin:/bin:/usr/local/bin:$::flyway::installer::install_dir",
    require     => Class['flyway']
  }


}
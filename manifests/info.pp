define flyway::info () {
  include ::flyway::installer

  # run some the flyway executable and create a file 
  exec { 'flyway_path_test':
    command     => 'flyway info',
    path        => "/usr/bin:/usr/sbin:/bin:/usr/local/bin:$::flyway::installer::install_dir",
    require     => Class['flyway']
  }
}
class flyway::installer ( 
  $version = $::flyway::params::version,
  $download_dir = $::flyway::params::download_dir,
  $path = $::flyway::params::path,
  $add_to_path = $::flyway::params::path
) inherits ::flyway::params {

  if(!Package['wget']){
    package { 'wget':
      ensure => installed,
    }
  }

   $tar_file = $operatingsystem ? {
      /(Ubuntu|Debian)/ => "flyway-commandline-${version}-linux-x64.tar.gz"
   }

   $download_url = $operatingsystem ? {
      /(Ubuntu|Debian)/ => "https://bintray.com/artifact/download/business/maven/${tar_file}",
   }

  exec { 'flyway_create_install_dir':
    command => "mkdir -p ${path}",
    path    => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
  }

  exec { 'flyway_download':
    path    => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
    command => "wget https://bintray.com/artifact/download/business/maven/${tar_file}",
    creates => "/tmp/${tar_file}",
    cwd     => '/tmp',
    unless  => "ls ${path}/flyway-${version}" ,
    require => Exec['flyway_create_install_dir'],
  }

  exec { 'flyway_extract':
    path    => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
    command => "tar -xf /tmp/${tar_file}",
    cwd     => "${path}",
    require => Exec['flyway_download'],
    unless  => "ls ${path}/flyway-${version}" ,
  }->exec{ "flyway_remove_archive" :
    path    => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
    command => "rm -f /tmp/${tar_file}"
  }

  $install_dir = "${path}/flyway-${version}"

  if($add_to_path){
    file_line {'flyway_add_path':
      line      => join(['export PATH=$PATH:',"${install_dir}"]),
      path      => "$::flyway::params::path_config_file",
      ensure    => 'present',
    }
  }

}
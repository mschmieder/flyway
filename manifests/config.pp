# possibel parameters are:
#     --> url
#     --> driver
#     --> user
#     --> password
#     --> schemas
#     --> table
#     --> locations
#     --> resolvers
#     --> jarDirs
#     --> sqlMigrationPrefix
#     --> sqlMigrationSeparator
#     --> sqlMigrationSuffix
#     --> encoding
#     --> encoding
#     --> placeholderReplacement
#     --> placeholders
#     --> placeholderSuffix
#     --> target
#     --> validateOnMigrate
#     --> cleanOnValidationError
#     --> baselineVersion
#     --> baselineDescription
#     --> baselineOnMigrate
#     --> outOfOrder
#     --> callbacks
define flyway::config (
$path = {},
$params = {} ) 
{
  # setup variable for template matching
  $_flyway_config_params=$params
  $config_file_path=$path

  #run some the flyway executable and create a file 
  file { "${config_file_path}":
    ensure  => file,
    content => template('flyway/flyway.conf.erb'),
  }
}
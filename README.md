# flyway puppet module
## Example

```
class { "flyway" :
  version => '3.2.1',
  path => '/tools/flyway/',
}

::flyway::config{ "flyway.conf" :
  path => join(["$::flyway::installer::install_dir","/flyway.conf"]),
  params =>
  {
    url => 'jdbc:mysql://localhost:3306/haccp',
    user => 'root',
    password => 'mysql'
  },
  require => Class['flyway']
}

::flyway::migrate { 'flyway_migrations':
  source => "puppet:///database/sql",
  target => join(["$::flyway::installer::install_dir","/sql"]),
  recurse => remote,  
  require => [Flyway::Config['flyway.conf'],Mysql::Grant['haccp']]
}
```